import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class App {
    public static void main(String[] args) throws Exception {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse("buchhaendler.xml");

            Element buchElement = (Element) doc.getElementsByTagName("titel").item(0);

            String titel = buchElement.getTextContent();

            System.out.println(titel);
            
        } catch (Exception e) {
            System.out.println("There is an Error: " + e.toString());
        }
    }
}
